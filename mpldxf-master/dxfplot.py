import matplotlib
import subprocess
from matplotlib import pyplot as plt
from mpldxf import FigureCanvasDxf
matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
plt.plot(range(10))
plt.savefig('myplot.dxf')
subprocess.Popen(["myplot.dxf"], shell=True)